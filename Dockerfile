FROM nginx:latest

COPY default.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/prueba
COPY . /var/www/prueba

CMD ["nginx", "-g", "daemon off;"]
